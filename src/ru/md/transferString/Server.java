package ru.md.transferString;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args){
        System.out.println("Сервер ждет клиента...");

        try (ServerSocket serversocket = new ServerSocket( 8080);
             Socket clientSocket = serversocket.accept();
             BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(clientSocket.getInputStream()));
             BufferedWriter outputStream = new BufferedWriter (new OutputStreamWriter(clientSocket.getOutputStream()))) {

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            String request;
            while ((request = bufferedReader.readLine()) != null) {
                System.out.println("Прислал клиент: " + request);
                outputStream.write(request + "\r");
                System.out.println("Отправлено клиенту: " + request);
                outputStream.flush();
            }
            System.out.println("Клиент отключился!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
