package ru.md.transferString;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(socket.getInputStream()));
             BufferedWriter bufferedWriter = new BufferedWriter (new OutputStreamWriter(socket.getOutputStream()))) {
            String response;
            System.out.println("Введите строку для отправки на сервер");
            while (!(response = scanner.nextLine()).equals("stop")){
                bufferedWriter.write(response + "\r");
                System.out.println("Отправлено серверу: " + response);
                bufferedWriter.flush();
                response = bufferedReader.readLine();
                System.out.println("Прислалал сервер: " + response);
                System.out.println("Введите строку для отправки на сервер");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
