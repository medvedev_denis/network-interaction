package ru.md.transferNumber;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws InterruptedException {
        try (Socket socket = new Socket("localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()){
            int response = 1;
            outputStream.write(response);
            System.out.println("Отправлено серверу: " + response);
            while ((response = inputStream.read()) != -1) {
                System.out.println("Прислалал сервер: " + response);
                if (response >= 10) {
                    break;
                }
                outputStream.write(++response);
                System.out.println("Отправлено серверу: " + response);
                outputStream.flush();
                Thread.sleep(2000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
