package ru.md.transferNumber;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Сервер ждет клиента...");

        try (ServerSocket serversocket = new ServerSocket( 8080);
            Socket clientSocket = serversocket.accept();
            InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream()) {

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            int request;
            while ((request = inputStream.read()) != -1) {
                System.out.println("Прислал клиент: " + request);
                outputStream.write(++request);
                System.out.println("Отправлено клиенту: " + request);
                outputStream.flush();
                Thread.sleep(2000);
            }
            System.out.println("Клиент отключился!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
